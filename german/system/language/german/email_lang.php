<?php

$lang['email_must_be_array'] = "Die E-Mail Validierung muss als Feld (array) übergeben werden.";
$lang['email_invalid_address'] = "Ungültige E-Mail Adresse: %s";
$lang['email_attachment_missing'] = "Der folgende E-Mail Anhang konnte nicht gefunden werden: %s";
$lang['email_attachment_unreadable'] = "Der folgende E-Mail Anhang konnte nicht geöffnet werden: %s";
$lang['email_no_recipients'] = "Es müssen Empfänger angegeben werden: An, Cc, oder Bcc";
$lang['email_send_failure_phpmail'] = "E-Mail konnte nicht mit PHP mail() gesendet werden. Der Server ist möglicherweise nicht dafür konfiguriert.";
$lang['email_send_failure_sendmail'] = "E-Mail konnte nicht mit PHP Sendmail gesendet werden. Der Server ist möglicherweise nicht dafür konfiguriert.";
$lang['email_send_failure_smtp'] = "E-Mail konnte nicht über SMTP gesendet werden. Der Server ist möglicherweise nicht dafür konfiguriert.";
$lang['email_sent'] = "Die Nachricht wurde erfolgreich über folgende Protokoll übermittelt: %s";
$lang['email_no_socket'] = "Es konnte kein Socket für Sendmail geöffnet werden. Bitte Einstellungen überprüfen.";
$lang['email_no_hostname'] = "Es wurde kein SMTP Host angegeben.";
$lang['email_smtp_error'] = "Folgender SMTP Fehler ist aufgetreten: %s";
$lang['email_no_smtp_unpw'] = "Fehler: Es muss ein SMTP Benutzername und Kennwort angegeben werden.";
$lang['email_failed_smtp_login'] = "Der AUTH LOGIN Befehl konnte nicht gesendet werden. Fehler: %s";
$lang['email_smtp_auth_un'] = "Benutzername konnte nicht authentifiziert werden. Fehler: %s";
$lang['email_smtp_auth_pw'] = "Passwort konnte nicht authentifiziert werden. Fehler: %s";
$lang['email_smtp_data_failure'] = "Daten konnten nicht gesendet werden: %s";
$lang['email_exit_status'] = "Statuscode für ausstieg: %s";

/* End of file email_lang.php */
/* Location: ./system/language/english/email_lang.php */
/* Translation by: Kovah - www.kovah.de */